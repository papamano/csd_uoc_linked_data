#!/bin/bash

function Validate {
    echo 'Validating' $1
    ttl $1
    echo ""
}

TARGET_DIR=.

for i in $TARGET_DIR/*.ttl;
do
    file=${i##*/}
    #filename=${file%.*}

    Validate $file
done