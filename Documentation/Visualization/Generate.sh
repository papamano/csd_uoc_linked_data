DIRECTORY=.

for i in $DIRECTORY/*.dot;
do
    file=${i##*/}
    filename=${file%.*}
    target="../Images/"$filename".png"

    dot -Tpng $file -o $target
done