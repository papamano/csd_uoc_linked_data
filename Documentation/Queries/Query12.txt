12) List of all programming languages that are taught in CSD courses with at least 3 prerequisites.

SELECT DISTINCT ?name
WHERE {
    {
        SELECT ?course, COUNT(?preq) AS ?prerequisites
        WHERE {
            ?course a vivo:Course ;
                    schema:sourceOrganization csd:Computer_Science_Department ;
                    vivo:hasPrerequisite ?preq .
        }
        GROUP BY(?course)
        HAVING(COUNT(?preq) > 2)
    }

    ?lang a schema:ComputerLanguage ;
          schema:subjectOf ?course ;
          rdfs:label ?name .
}