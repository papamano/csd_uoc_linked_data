2) List of all courses in which the course HY-252 is a prerequisite.

SELECT DISTINCT ?course
WHERE {
    ?course a vivo:Course ;
            vivo:hasPrerequisite csd:CS_252 .
}