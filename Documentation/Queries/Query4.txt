4) In how many courses is each programming language used? (In descending order)

SELECT ?lang, count(?course) AS ?uses
WHERE {
    ?course a schema:Course ;
            schema:programmingLanguage ?lang .
    ?lang a schema:ComputerLanguage .
}
GROUP BY (?lang)
ORDER BY DESC(?uses)