* Αρχικα εφτιαξα ενα προγραμμα σε **Java** για να κάνω parse τα JSON αρχεία. H υλοποίηση μου χρησιμοποιεί το library GSON και **reflection** για να κανει το deserialization.
Μετα καταλαβα οτι επειδή δεν είναι όλα τα δεδομένα σε JSON format θα χρειαστώ μια scripting γλώσσα για να τα μετατρέψω σε JSON οπότε αποφάσισα να χρησιμοποιήσω κατευθειάν **JavaScript**.

* Μετέτρεψα το αρχείο **classrooms.txt** σε CSV format και με UTF-8 encoding.

* Μετέτρεψα το αρχείο **CSD-coursesProgrammingLanguage.csv** σε πραγματικό CSV γιατί ήταν μισό TSV και άλλο μισό CSV. Τo format του ήταν:
CLASS\tLanguage1,Language2

* Θα χρησιμοποιήσω και το VoiD ontology σίγουρα γιατί θέλω να προσθέσω metainformation

* Βρήκα ontology specifally for Computer Science Department αλλά αποφάσισα πως είναι καλύτερο να χρησιμοποιήσω το University Ontology για να έχω πιο generic σύστημα που μπορεί να χρησιμοποιηθεί και για τα άλλα τμήματα. Άλλωστε το CSD Ontology έκανε extend το University Ontology και δεν πρόσθετε πολλά πράγματα.

* Κοίταξα τις CRM ontologies αλλά κανένα δεν μου έκανε.

* Από το Protege Ontology Library βρήκα το παρακάτω ontology που μου κάνει. Institutional Οntology (https://www.isibang.ac.in/~bisu/ontology/instOntology.owl)

* Θέλω να χρησιμοποιήσω και τα ontologies foaf και schema τα οποία είναι πολύ γνωστά και χρησιμοποιούνται παντού.

* Το UniversityOntology ήταν καλό αλλά είναι μόνο draft. Γι' αυτό κατέληξα στο VIVO το οποίο έχει σχεδόν όλα όσα έχει το UniversityOntology και σίγουρα πολλά περισσότερα που χρειάζομαι

* Για να μετατρέψω το SchemaMapping.ttl σε valid αρχείο τρέχω:
```bash
cpp SchemaMapping.ttl -undef -P > Base.ttl
```

* How to generate triples:
```bash
cd Triples
node DepartmentParser.js > ../Triples/Department.ttl
node ClassroomsParser.js > ../Triples/Classrooms.ttl
node CoursesParser.js > ../Triples/Courses.ttl
node TopicsParser.js > ../Triples/Topics.ttl
node LanguagesParser.js > ../Triples/ProgrammingLanguages.ttl
node PersonsParser.js > ../Triples/Persons.ttl
node ServicesParser.js > ../Triples/Services.ttl
node BooksParser.js > ../Triples/Books.ttl
node TeachingsParser.js > ../Triples/Teachings.ttl
```

* Για το validation των TTL αρχείων (RDF triples) χρησιμοποιώ το εργαλείο **Turtle Validator** http://ttl.summerofcode.be/
Το χρησιμοποιώ ως CLI tool. Για εγκατάσταση:
```
npm install -g turtle-validator
```
Για εκτέλεση:
```
ttl Triples/Courses.ttl
```

* To delete a graph from Virtuoso:

In **iSQL** tab execute:
```
grant SPARQL_UPDATE to "SPARQL";
```
In **Sparql** tab execute:
```
CLEAR GRAPH <http://localhost:8890/CSD>
```

* To find Virtuoso predefined Predixes run the following in **iSQL**:
```
SELECT * FROM DB.DBA.SYS_XML_PERSISTENT_NS_DECL;
```

* To count lines of code for server:
```bash
cloc --exclude-dir=node_modules .
```

* Για το client side visualization χρησιμοποιώ το **vis.js** https://visjs.org/ και συγκεκριμένα το Network module