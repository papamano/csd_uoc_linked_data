# Tasks:

1. Department ✓
2. University ✓
3. Classrooms ✓
4. Course ✓
5. Area ✓
6. Topic ✓
7. Programming Languages ✓
8. Persons ✓
9. Services ✓
10. Books ✓
11. Teachings ✓
12. Classification ✓

**12/12**

---------------
# Computer Science Department

## Ontology Extensions:

* *None required*

## Mapping:

* The type (**rdf:type**) of the entity will be **vivo:AcademicDepartment** and **schema:Organization**

* The **name** will be represented using the property **rdfs:label**

* The **address** will be represented using the properties **vcard:hasAddress** and **schema:address**. The object of the triple will be an entity of type **vcard:Address** and **schema:PostalAddress** using the properties **rdfs:label**, **vcard:country**/**schema:addressCountry**, **vcard:postalCode**/**schema:postalCode**, **vcard:region**/**schema:addressRegion** and **vcard:streetAddress**/**schema:streetAddress**

* The **phone** will be represented using the properties **schema:telephone** and **vcard:telephone**.

* The **email** will be represented using the property **schema:email**

* The **URL** will be represented using the properties **schema:url** and **vcard:url** and **foaf:homepage**

* The **fax number** will be represented using the property **schema:faxNumber**.

* The **hours** will be represented using the property **schema:openingHours**

* The **campus** will be represented using the properties **obo:RO_0001018** ("contained in") and **schema:containedInPlace**

* The **map** will be represented using the property **schema:hasMap**

* The **logo** will be represented using the property **schema:logo**

* The **photo** will be represented using the property **schema:photo**

* The relationship that CSD is part of UoC will be represented using the properties **schema:subOrganization** and **schema:memberOf** and **vivo:hasGoverningAuthority** and **schema:memberOf** and **schema:parentOrganization**

## Required Instances:

* **manos:Greece** a schema:Country ; a vivo:Country ;

* **manos:Voutes_Campus_Address** a **vcard:Address** ; a **schema:PostalAddress** .

* **manos:Computer_Science_Department** a vivo:AcademicDepartment .

* **manos:Voutes_Campus** a vivo:Campus ; a schema:Place ; rdfs:label "Πανεπιστημιούπολη Βουτών" ; vcard:hasAddress manos:Voutes_Campus_Address .

---------------
# University of Crete

* The type (**rdf:type**) of the entity will be **vivo:vivo:University** and **schema:CollegeOrUniversity** and **schema:Organization**

* The **name** will be represented using the property **rdfs:label**

* The **address** will be represented using the properties **vcard:hasAddress** and **schema:address**.

* The **phone** will be represented using the properties **schema:telephone** and **vcard:telephone**.

* The **email** will be represented using the property **schema:email**

* The **URL** will be represented using the properties **schema:url** and **vcard:url** and **foaf:homepage**

* The **fax number** will be represented using the property **schema:faxNumber**.

* The **hours** will be represented using the property **schema:openingHours**

* The **campus** will be represented using the properties **obo:RO_0001018** ("contained in") and **schema:containedInPlace**

* The **map** will be represented using the property **schema:hasMap**

* The **logo** will be represented using the property **schema:logo**

* The **photo** will be represented using the property **schema:photo**

## Ontology extensions:

* *None required*

## Mapping:

## Required Instances:

* **manos:University_of_Crete** a vivo:University ; a schema:CollegeOrUniversity .

---------------
# Classroom #

## Ontology extensions:

* manos:**Classroom** a owl:Class ; rdfs:subClassOf **vivo:Room** ; rdfs:subClassOf **schema:Room** .

## Mapping:

* The type (**rdf:type**) of each classroom will be: **manos:Classroom**

* The **seating capacity** will be represented using the properties **vivo:seatingCapacity** and **schema:maximumAttendeeCapacity**. The type of each object in the triple will be **xsd:integer**

* The **name** will be represented using the property **rdfs:label**

* Each entity will have the property **schema:containedInPlace** with manos:Voutes_Campus as object

## Required Instances:

* *None required*

---------------
# Course #

## Ontology extensions:

* manos:**AcademicProgram** a owl:Class ; rdfs:subClassOf vivo:Program .

* manos:**suggestedFor** a owl:ObjectProperty ; rdfs:domain vivo:Course ; rdfs:label "suggested for" ; rdfs:range vivo:Course .

* manos:**hasSuggested** a **owl:ObjectProperty** ; rdfs:domain vivo:Course ; rdfs:label "has suggested" ; rdfs:range vivo:Course ; owl:inverseOf manos:suggestedFor .

* manos:**hasProgram** a owl:ObjectProperty ; rdfs:domain vivo:Course ; rdfs:label "has academic program" ; rdfs:range manos:AcademicProgram .

* manos:**CourseArea** a owl:Class ; rdfs:subClassOf skos:Concept .

## Mapping:

* The type (**rdf:type**) of each course will be: **vivo:Course** and **schema:ContactPoint** and **schema:Course**

* The **course name** will be represented using the property **rdfs:label** (with language tags)

* The **course text** will be represented using the properties **rdfs:comment** and **rdfs:abstract** and **schema:description**

* The **course code** will be represented using the properties **schema:courseCode** and **bibo:identifier**

* The **course external URL** will be represented using the properties **vcard:url** and **schema:url** and **foaf:homepage**

* The **course email** will be represented using the property **schema:email**

* The **ECTS** will be represented using the properties **vivo:courseCredits**

* The required courses will be represented using the properties **vivo:hasPrerequisite** and **schema:coursePrerequisites**. Each required class will be linked with the original course (the one beeing currently processed) using the property **vivo:prerequisiteFor**

* The suggested courses will be represented using the property **manos:hasSuggested**. Each suggested class will be linked with the original coure (the one beeing currently processed) using the property **manos:suggestedFor**

* The **program name** will be represented using the property **manos:hasProgram**. The object of this triple will be either manos:**UndergraduateProgram** or manos:**GraduateProgram**

* Both **area code** and **area name** will be represented using the property **vivo:hasSubjectArea**. The object of the triple will be an entity of type manos:CourseArea. The area code will be represented using the property **bibo:identifier** while the area name will be represented using the property **rdfs:label**. The area will be linked with the course using the property **vivo:subjectAreaOf**.

* Each course will have the properties (*vivo:hasGoverningAuthority*) and **schema:sourceOrganization** with manos:Computer_Science_Department as an object

## Required Instances:

* manos:**UndergraduateProgram** a manos:AcademicProgram .

* manos:**GraduateProgram** a manos:AcademicProgram .

---------------
# Topics #

## Ontology extensions:

* manos:**Topic** a owl:Class ; rdfs:subClassOf **skos:Concept** .

## Mapping:

* The **topics** will be represented using the properties **vivo:hasAssociatedConcept** and **schema:about**. Each topic will be an entity with type manos:Topic

## Required Instances:

* *None required*

---------------
# Programming Languages #

## Ontology extensions:

* *None required*

## Mapping:

* The **language** will be represented using the property **schema:programmingLanguage**. Each language will be linked with the course using the property **schema:subjectOf**

* Each language will be an entity with type **schema:ComputerLanguage**

## Required Instances:

* *None required*

---------------
# Persons #

## Ontology extensions:

* manos:**Professor** a owl:Class ; rdfs:subClassOf **vivo:FacultyMember** ; rdfs:subClassOf **schema:Person** .

* manos:**AssistantProffesor** a owl:Class ; rdfs:subClassOf **manos:Professor** .

* manos:**AssociateProfessor** a owl:Class ; rdfs:subClassOf **manos:Professor** .

* manos:**FullProfessor** a owl:Class ; rdfs:subClassOf **manos:Professor** .

* manos:**VisitingProfessor** a owl:Class ; rdfs:subClassOf **manos:Professor** .

* manos:**AdministrativeStaff** a owl:Class ; rdfs:subClassOf **vivo:FacultyMember** ; rdfs:subClassOf **schema:Person** .

* manos:**SecretaryStaff** a owl:Class ; rdfs:subClassOf **manos:AdministrativeStaff** .

* manos:**LabStaff** a owl:Class ; rdfs:subClassOf **manos:AdministrativeStaff** .

* manos:**SystemStaff** a owl:Class ; rdfs:subClassOf **manos:AdministrativeStaff** .

## Mapping:

* Each person will be an entity with types **vivo:FacultyMember** (subclass of Person) and **schema:Person**

* The **name** will be represented using the property **schema:name**. The name can be split in 2 and use the properties **schema:givenName** for first name and **schema:familyName** for last name.

* The **email** will be represented using the property **schema:email**

* The **URL** will be represented using the properties **schema:url** and **vcard:url** and **foaf:homepage** and **foaf:workplaceHomepage**

* The **text** will be represented using the properties **rdfs:abstract** and **schema:description**

* Each person will have the properties **schema:worksFor** and **schema:affiliation** and **schema:memberOf** with **manos:Computer_Science_Department** and **manos:University_of_Crete** as objects

* The **position name** will define the **rdf:type** and **vcard:hasRole** and **vivo:contributingRole** properties

## Required Instances:

* manos:**ProfessorPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**AssistantProffesorPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**AssociateProfessorPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**FullProfessorPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**VisitingProfessorPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**AdministrativeStaffPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**SecretaryStaffPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**LabStaffPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .

* manos:**SystemStaffPosition** rdf:type vivo:FacultyPosition ; rdf:type obo:ERO_0000788 ; rdf:type vcard:Role .


---------------
# Services #

## Ontology extensions:

* *None required*

## Mapping:

* Each service will be an entity with **rdf:type** **schema:Service**

* The **category** will define the type (**rdf:type**) of the entity. The available options are:
    * obo:ERO_0000391 (Access Service)
    * obo:ERO_0001258 (Maintenance Service)
    * obo:ERO_0000392 (Storage Service)
    * obo:ERO_0001257 (Data Storage Service)
    * obo:ERO_0001255 (Support Service)
    * obo:ERO_0000393 (Training Service)
    * obo:ERO_0001254 (Transport Service)

* The **title** will be represented using the property **rdfs:label** .

* The **description** will be represented using the properties **rdfs:comment** and **rdfs:abstract** and **schema:description**

* The **URL** will be represented using the properties **vcard:url** and **schema:url** and **foaf:homepage**

* Each service will have the properties **schema:provider** and **obo:ERO_0000390** with **manos:Computer_Science_Department** as an object

* For each service there will be the triple **manos:Computer_Science_Department obo:ERO_0000037** service

## Required Instances:

* *None required*

---------------
# Books #

## Ontology extensions:

* **manos:textbook** a owl:ObjectProperty .

(Used since schema:teaches is in the PENDING version of the ontology)

* **manos:wrote** a owl:ObjectProperty .

## Mapping:

* The type (**rdf:type**) of each book will be: **bibo:Book** and **schema:Book**

* The **title** will be represented using the property **rdfs:label**

* The **edition** will be represented using the properties **schema:bookEdition** and **bibo:Edition**

* The **date** will be represented using the property **schema:datePublished**

* The **isbn** will be represented using the properties **schema:isbn** and **bibo:identifier**. Based on the length of the string the properties **bibo:isbn10** and **bibo:isbn13** can be used too.

* The **course** will be represented using the property **manos:textbook**.

* The **author** will be represented using the property **schema:author**. Each author will be linked with its book using the property **manos:authorOf**

## Required Instances:

* *None required*

---------------
# Teachings #

## Ontology extensions:

* manos:**hasAcademicYear** a owl:ObjectProperty .

* manos:**hasAcademicSemester** a owl:ObjectProperty .

* manos:**hasTeachingAssistant** a owl:ObjectProperty ; rdfs:subPropertyOf <http://www.obofoundry.org/ro/ro.owl#has_agent> .

* manos:**AcademicSemester** a owl:Class ; rdfs:subClassOf vivo:AcademicTerm .

## Mapping:

* The type (**rdf:type**) of each teaching will be **schema:CourseInstance**

* The **course** will be represented using the property **schema:about**

* The **year** will be represented using the property **manos:hasAcademicYear**. Each academic year will be an entity with type **vivo:AcademicYear**

* The **semester** will be represented using the property **manos:hasAcademicSemester**

* The **tutor** will be represented using the property **schema:instructor**. Each tutor will be the subject of the triples rdf:type Person and vivo:role TeachingRole .

* The **assistant** will be represented using the property **manos:hasTeachingAssistant**. Each assistant will be an entity with types **foaf:Person** and **schema:Person** and will have the property **vivo:contributingRole** with **obo:ERO_0000785** (graduate student role) as an object

* The **room** will be represented using the property **schema:location**

* The **hours** will be represented using the property **vivo:dateTimeValue** . Each time will be an entity with type **vivo:DateTimeValue**

* The **syllabus** will be represented using the properties **rdfs:abstract** and **vivo:teachingOverview** and **schema:description**

* Each course will be linked with the respective teaching using the property **schema:hasCourseInstance**

## Required Instances:

* manos:**SpringSemester** a manos:AcademicSemester .

* manos:**FallSemester** a manos:AcademicSemester .

---------------
# Computer Science Department

## Ontology extensions:

* **manos:acmClassification** a owl:ObjectProperty ; rdfs:subPropertyOf vivo:hasAssociatedConcept .

## Mapping:

* Each course will be linked to the respective ACM class using the property **manos:acmClassification**

## Required Instances:

* *None required*

---------------
# Depricated: #

* manos:**courseCode** a owl:DatatypeProperty ; rdfs:label "course code" ; rdfs:subPropertyOf bibo:identifier ; rdfs:domain vivo:Course .

* manos:**time** a owl:DatatypeProperty .