# CSD UoC Linked Data
# HY-561: Web Data Management - Project

## Information

This repository is for the project of the course HY-561: Web Data Management. It will be used as following:

* The master branch will contain the latest stable version of the project.
* The master branch will only contain, at any time, the **final** version of the project. Versions that arise during development will stay in their branches until they are finished. Therefore, the code that will be submited for evaluation will be the code in the master branch.
* **Tags** will be used to indicate the process of development.

## Prerequisites

Required software:

* NodeJs v8.10.0
* npm v3.5.2

Required hardware:

* N/A

Required Files:

* *See Project Stucture*.

## Installation

To download project:
```bash
git clone https://gitlab.com/papamano/csd_uoc_linked_data.git
```

To generate RDF triples:
```bash
cd Parsers
chmod +x ./Generate.sh
./Generate.sh
```

To validate RDF triples:
```bash
cd Triples
chmod +x ./Validate.sh
./Validate.sh
```

To install server:
```bash
cd Server
npm install
```

To run server:
```bash
cd Server
node app.js
```

## Project Structure
```
.
├── Datasets/
├── Documentation/
├── Facetize/
├── Ontologies/
├── Parsers/
├── Server/
├── Triples/
├── .gitignore
├── Makefile
├── README.md
└── Report.pdf
```

* Datasets: Input data.
* Documentation: Project documentation.
* Facetize: Entire projects of Facetize app
* Ontologies: Ontologies on top of which the project is built.
* Parsers: Source code that generates RDF triples from datasets.
* Server: Web application based on the triplestore
* Triples: Generated RDF triples .

## Notes

* Please note that the makefile is custom-made so it might fail under conditions that have not been tested. In such cases please build the project by hand.

## Lines of Code

Parsers:
* 1125 code
* 329 blank
* 38 comment
* 13 files

Web application:
* 1040 code
* 234 blank
* 66 comment
* 17 files

Testing:
*(Not available yet)*

## Roadmap

See the open issues for a list of proposed features (and known issues).

## Contributors / Authors

* Papadogiannakis Manos

## Support

Please contact one of the project's contributors.

If you need an expert:
Yannis Tzitzikas

## License

Closed source. **Proprietary and confidential**.
Unauthorized copying of this project, via any medium is strictly prohibited.