# Current directory
PROJECTDIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# Project structure
TRIPLES_DIR=Triples
PARSERS_DIR=Parsers
SERVER_DIR=Server

# Used files
GENERATE_SCRIPT=./Generate.sh
VALIDATE_SCRIPT=./Validate.sh

all: generate validate server_init
	@echo 'Project ready'

generate:
	@cd $(PARSERS_DIR); chmod +x $(GENERATE_SCRIPT); $(GENERATE_SCRIPT)

validate:
	@cd $(TRIPLES_DIR); chmod +x $(VALIDATE_SCRIPT); $(VALIDATE_SCRIPT)

server_init:
	@echo 'Installing server requirements...'
	@cd $(SERVER_DIR); npm install

server_start:
	@echo 'Running server...'
	@cd $(SERVER_DIR); node app.js