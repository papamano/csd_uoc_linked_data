const fs = require("fs")

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

function ClassroomIsValid(room) {
	assert(typeof(room) == "object");

	result = typeof (room.name) == "string" &&
			 typeof (room.label) == "string" &&
			 typeof (room.size) == "string";

	if (!result) return false;

	/* Condition check */
	result = room.name.length >= 4 &&
			 room.label.length >= 4 &&
			 room.size.length >= 2;

	return result;
}

function RoomToRDF(room) {
	assert(typeof(room) == "object");

	PrintRDF(Sanitize(room.name), "rdf:type csd:Classroom", ";");
	PrintRDF(TAB, "rdfs:label", Stringify(room.label, LANGS.GR), ";");
    PrintRDF(TAB, "vivo:seatingCapacity", room.size, ";");
    PrintRDF(TAB, "schema:containedInPlace", "csd:Voutes_Campus", ";");
	PrintRDF(TAB, "schema:maximumAttendeeCapacity", room.size, ".");
}

function Classrooms(path) {
	assert(typeof (path) == "string" && path);

	try {
		const text = fs.readFileSync(path, "utf8");
		const data = text.split("\n");

		let entries = 0;

		DumpPrefixes();
		NewLine();

		data.forEach(line => {
			/* Data cleaning */
			line = line.replace("\r", "");

			diagnostics("Processing entry ", line);

			let columns = line.split(",");
			let classroom = {
				"name"  : columns[0],
				"label" : columns[1],
				"size"  : columns[2]
			};

			assert(ClassroomIsValid(classroom));
			TrimObjectStrings(classroom);
			RoomToRDF(classroom);
			NewLine();

			entries += 1;
		});

		/* Statistics */
		console.error("\nSuccessfully parsed: ");
		console.error("Classrooms:", entries);

	} catch (err) {
		console.error("Parsing failed: ", err);
	}
}


const CLASSROOMS_PATH = "../Datasets/classrooms.csv";

Classrooms(CLASSROOMS_PATH);