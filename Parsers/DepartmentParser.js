const fs = require("fs")
const Utils = require("./Utilities.js");

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    Urlify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

const DATA_PATH = "../Datasets/Department.json";

function EntryIsValid(entry) {
    assert(typeof(entry) == "object");

	/* Type check */
	result = typeof(entry.name) == "string" &&
			 typeof(entry.name_en) == "string" &&
			 typeof(entry.address) == "string" &&
             typeof(entry.address_en) == "string" &&
             typeof(entry.address_en) == "string" &&
             typeof(entry.phone) == "string" &&
             typeof(entry.fax) == "string" &&
             typeof(entry.email) == "string" &&
             typeof(entry.url) == "string" &&
             typeof(entry.openingHours) == "string" &&
             typeof(entry.campus) == "string" &&
             typeof(entry.logo) == "string" &&
             typeof(entry.map) == "object" &&
			 typeof(entry.photo) == "object";

    if (!result) return false;

	/* Condition check */
    result = entry.name.length > 0 &&
             entry.name_en.length > 0 &&
             entry.address.length > 0 &&
             entry.address_en.length > 0 &&
             entry.phone.length > 0 &&
             entry.fax.length > 0 &&
             entry.email.length > 0 &&
             entry.url.length > 0 &&
             entry.openingHours.length > 0 &&
             entry.campus.length > 0 &&
             entry.map.length > 0 &&
             entry.logo.length > 0 &&
             entry.photo.length > 0;

	return result;
}

function EntryToRDF(entry, subject, bothCampuses) {
    assert(EntryIsValid(entry));
    assert(typeof(subject) == "string" && subject.length > 0);

    PrintRDF(subject, "rdfs:label", Stringify(entry.name, LANGS.GR), ";");
    PrintRDF(TAB, "rdfs:label", Stringify(entry.name_en, LANGS.EN), ";");
    PrintRDF(TAB, "schema:telephone", Stringify(entry.phone), ";");
    PrintRDF(TAB, "vcard:telephone", Stringify(entry.phone), ";");
    PrintRDF(TAB, "schema:email", Urlify(entry.email), ";");
    PrintRDF(TAB, "schema:url", Urlify(entry.url), ";");
    PrintRDF(TAB, "vcard:url", Urlify(entry.url), ";");
    PrintRDF(TAB, "foaf:homepage", Urlify(entry.url), ";");
    PrintRDF(TAB, "schema:openingHours", Stringify(entry.openingHours), ";");
    PrintRDF(TAB, "obo:RO_0001018", "csd:Voutes_Campus", ";");
    PrintRDF(TAB, "schema:containedInPlace", "csd:Voutes_Campus", ";");

    if (typeof(bothCampuses) == "boolean" && bothCampuses) {
        PrintRDF(TAB, "obo:RO_0001018", "csd:Rethymno_Campus", ";");
        PrintRDF(TAB, "schema:containedInPlace", "csd:Rethymno_Campus", ";");
    }

    entry.map.forEach(m => PrintRDF(TAB, "schema:hasMap", Urlify(m), ";"));
    entry.photo.forEach(p => PrintRDF(TAB, "schema:photo", Urlify(p), ";"));

    PrintRDF(TAB, "schema:logo", Urlify(entry.logo), ".");
}

function DepartmentToRDF(department) {
    assert(typeof(department) == "object");
    assert(EntryIsValid(department));

    EntryToRDF(department, "csd:Computer_Science_Department");
}

function UniversityToRDF(university) {
    assert(typeof(university) == "object");
    assert(EntryIsValid(university));

    EntryToRDF(university, "csd:University_of_Crete", true);
}

(function main(path) {
    assert(typeof (path) == "string" && path);

	try {
        const text = fs.readFileSync(path, "utf8");
        const data = JSON.parse(text);

        assert(data.length == 2);
        assert(EntryIsValid(data[0]));
        assert(EntryIsValid(data[1]));

        DumpPrefixes();
        NewLine();
        DepartmentToRDF(data[0]);
        NewLine();
        UniversityToRDF(data[1]);
    } catch (err) {
		console.error("Parsing failed: ", err);
    }

})(DATA_PATH);