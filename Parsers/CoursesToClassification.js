const fs = require("fs")

const COURSES_PATH = "../Datasets/CSD-courses.json";

let result = [];

try {
    const text = fs.readFileSync(COURSES_PATH, "utf8");
    const data = JSON.parse(text);

    data.forEach(course => {
        result.push({
            "course" : course.course_code_en,
            "description" : "this_is_the_description",
            "class" : [ "this_is_the_class" ],
        });
    });

} catch (err) {
    console.error("Parsing failed: ", err);
}

fs.writeFileSync("classification.json", JSON.stringify(result));