const fs = require("fs")

const {
    assert,
    Sanitize,
    TrimObjectStrings,
    diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

function CourseIsValid(course) {
    /* Type check */
    result = typeof(course.course) == "string" &&
             typeof(course.year) == "string" &&
             typeof(course.semester) == "string" &&
             typeof(course.tutor) == "string" &&
             typeof(course.room) == "string" &&
             typeof(course.hours) == "string" &&
             typeof(course.syllabus) == "string" &&
             typeof(course.assistants) == "object";

    if (!result) return false;

    /* Condition check */
    result = (course.course.length == 6 || course.course.length == 9) &&
             course.year.length == 4 &&
             (course.semester == "winter" || course.semester == "spring") &&
             course.tutor.length > 0 &&
             course.room.length >= 4 &&
             course.hours.length >= 4;

    return result;
}

function FormAcademicYear(year) {
    assert(typeof(year) == "string" && year.length == 4);
    //return "Academic_Year_" + year + "-" + (parseInt(year) + 1);
    return year + "-" + (parseInt(year) + 1);
}

function TeachingToRDF(course) {
    assert(CourseIsValid(course));

    let teaching = course.course + "_" + FormAcademicYear(course.year);

    PrintRDF(Sanitize(teaching), "rdf:type schema:CourseInstance", ";");
    PrintRDF(TAB, "schema:about", Sanitize(course.course), ";");
    PrintRDF(TAB, "csd:hasAcademicYear", Sanitize(FormAcademicYear(course.year)), ";");
    PrintRDF(TAB, "schema:instructor", Sanitize(course.tutor), ";");
    PrintRDF(TAB, "schema:location", Sanitize(course.room), ";");
    PrintRDF(TAB, "vivo:dateTimeValue", Sanitize(course.hours), ";");

    if (course.semester == "winter")
        PrintRDF(TAB, "csd:hasAcademicSemester csd:FallSemester", ";");
    else if (course.semester == "spring")
        PrintRDF(TAB, "csd:hasAcademicSemester csd:SpringSemester", ";");
    else
        assert(false);

    course.assistants.forEach(a => PrintRDF(TAB, "csd:hasTeachingAssistant", Sanitize(a), ";"));

    if (course.syllabus) {
        let lang = (course.syllabus.includes("e") ? LANGS.EN : LANGS.GR);
        PrintRDF(TAB, "rdfs:abstract", Stringify(course.syllabus, lang), ";");
        PrintRDF(TAB, "vivo:teachingOverview", Stringify(course.syllabus, lang), ";");
        PrintRDF(TAB, "schema:description", Stringify(course.syllabus, lang), ";");
    }

    let label = "Teaching of course " + course.course + " (academic year " + FormAcademicYear(course.year) + ")";
    PrintRDF(TAB, "rdfs:label", Stringify(label, LANGS.EN), ".");

    PrintRDF(Sanitize(course.course), "schema:hasCourseInstance", Sanitize(teaching), ".");
}

function TutorToRDF(tutor) {
    assert(typeof(tutor) == "string");

    PrintRDF(Sanitize(tutor), "rdf:type foaf:Person", ";");
    PrintRDF(TAB, "rdf:type schema:Person", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(tutor, LANGS.EN), ";");
    PrintRDF(TAB, "vivo:contributingRole vivo:TeacherRole", ".")

    NewLine();
}

function AssistantToRDF(assistant) {
    assert(typeof(assistant) == "string");

    PrintRDF(Sanitize(assistant), "rdf:type foaf:Person", ";");
    PrintRDF(TAB, "rdf:type schema:Person", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(assistant, LANGS.EN), ";");
    PrintRDF(TAB, "schema:givenName", Stringify(assistant.split(" ")[0].trim(), LANGS.EN), ";");
    PrintRDF(TAB, "schema:familyName", Stringify(assistant.split(" ")[1].trim(), LANGS.EN), ";");
    PrintRDF(TAB, "schema:affiliation", "csd:Computer_Science_Department", ";");
    PrintRDF(TAB, "schema:affiliation", "csd:University_of_Crete", ";");
    PrintRDF(TAB, "vivo:contributingRole", "vivo:ResearcherRole", ";");
    PrintRDF(TAB, "vivo:contributingRole", "obo:ERO_0000785", ";");
    PrintRDF(TAB, "vivo:contributingRole", "obo:ERO_0000225", ".");

    NewLine();
}

function AcademicYearToRDF(year) {
    assert(typeof(year) == "string");

    PrintRDF(Sanitize(FormAcademicYear(year)), "rdf:type vivo:AcademicYear", ";");
    PrintRDF(TAB, "rdfs:label", Stringify("Ακαδημαϊκό έτος " + FormAcademicYear(year), LANGS.GR), ";");
    PrintRDF(TAB, "rdfs:label", Stringify("Academic year " + FormAcademicYear(year), LANGS.EN), ".");

    NewLine();
}

function HoursToRDF(hour) {
    assert(typeof(hour) == "string");

    PrintRDF(Sanitize(hour), "rdf:type vivo:DateTimeValue", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(hour, LANGS.EN), ".");

    NewLine();
}

function Teachings(path) {
    assert(typeof (path) == "string" && path);

    try {
        const text = fs.readFileSync(path, "utf8");
        const data = JSON.parse(text);

        let courses = 0;
        let tutors = [];
        let assistants = [];
        let years = [];
        let hours = [];

        DumpPrefixes();
        NewLine();

        data.forEach(course => {
            diagnostics("Processing course: ", course.course);
            assert(CourseIsValid(course));
            TrimObjectStrings(course);
            TeachingToRDF(course);
            NewLine();

            courses += 1;
            tutors.push(course.tutor);
            years.push(course.year);
            hours.push(course.hours);
            assistants = assistants.concat(course.assistants);
        });

        /* Processing of tutors as entities */
        tutors = RemoveDuplicates(tutors);
        tutors.forEach(TutorToRDF);

        /* Processing of assistants as entities */
        assistants = RemoveDuplicates(assistants);
        assistants.forEach(AssistantToRDF);

        years = RemoveDuplicates(years);
        years.forEach(AcademicYearToRDF);

        hours = RemoveDuplicates(hours);
        hours.forEach(HoursToRDF);

        /* Statistics */
        console.error("\nSuccessfully parsed: ");
        console.error("Courses:", courses);
        console.error("Instructors:", tutors.length);
        console.error("Assistants:", assistants.length);
        console.error("Academic Years:", years.length);
        console.error("Hours:", hours.length);

    } catch (err) {
        console.error("Parsing failed: ", err);
    }
}

const TEACHINGS_PATH = "../Datasets/Teachings.json";

Teachings(TEACHINGS_PATH);