const fs = require("fs")
const Utils = require("./Utilities.js");

const {
    assert,
    Sanitize,
    TrimObjectStrings,
    diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

function BookIsValid(book) {
    /* Type check */
    result = typeof(book.title) == "string" &&
             typeof(book.edition) == "string" &&
             typeof(book.date) == "string" &&
             typeof(book.isbn) == "string" &&
             typeof(book.course) == "string" &&
             typeof(book.year) == "string" &&
             typeof(book.authors) == "object";

    if (!result) return false;

    /* Condition check */
    result = book.title.length > 0 &&
             (book.edition.length == 0 || book.edition.length == 3 || book.edition.length == 4) &&
             (book.date.length == 0 || book.date.length == 4) &&
             (book.course.length == 6 || book.course.length == 9) &&
             book.year.length == 4 &&
             book.authors.length > 0;

    return result;
}

function FormAcademicYear(year) {
    assert(typeof(year) == "string" && year.length == 4);
    return year + "-" + (parseInt(year) + 1);
}

function BookTeachingToRDF(book) {
    assert(BookIsValid(book));

    let teaching = book.course + "_" + FormAcademicYear(book.year);

    PrintRDF(Sanitize(teaching), "csd:textbook", Sanitize(book.title), ".");
    NewLine();
}

function FormatIsbn(str) {
    assert(typeof(str) == "string" && str.length > 0);
    return str.replace(/-/g, "");
}

function BookToRDF(book) {
    assert(typeof(book) == "string");

    book = JSON.parse(book);

    PrintRDF(Sanitize(book.title), "rdf:type schema:Book", ";");
    PrintRDF(TAB, "rdf:type bibo:Book", ";");

    if (book.edition) {
        PrintRDF(TAB, "schema:bookEdition", Stringify(book.edition, LANGS.EN), ";");
        PrintRDF(TAB, "bibo:Edition", Stringify(book.edition, LANGS.EN), ";");
    }

    if (book.date) {
        PrintRDF(TAB, "schema:datePublished", book.date, ";");
    }

    if (book.isbn) {
        isbn = FormatIsbn(book.isbn);
        if (isbn.length == 10)
            PrintRDF(TAB, "bibo:isbn10", Stringify(isbn), ";");
        else if (isbn.length == 13)
            PrintRDF(TAB, "bibo:isbn13", Stringify(isbn), ";");
        else assert(false);

        PrintRDF(TAB, "bibo:identifier", Stringify(isbn), ";");
    }

    PrintRDF(TAB, "rdfs:label", Stringify(book.title, LANGS.EN), ".");

    NewLine();
}

function AuthorToRDF(author) {
    assert(typeof(author) == "string");

    PrintRDF(Sanitize(author), "rdf:type foaf:Person", ";");
    PrintRDF(TAB, "rdf:type schema:Person", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(author, LANGS.EN), ";");
    PrintRDF(TAB, "foaf:name", Stringify(author, LANGS.EN), ".");
    NewLine();
}

function AuthorBookToRDF(pair) {
    assert(typeof(pair) == "object");
    assert(typeof(pair.author) == "string");
    assert(typeof(pair.book) == "string");

    PrintRDF(Sanitize(pair.book), "schema:author", Sanitize(pair.author), ".");
    PrintRDF(Sanitize(pair.author), "csd:authorOf", Sanitize(pair.book), ".");
    NewLine();
}

function Books(path) {
    assert(typeof (path) == "string" && path);

    try {
        const text = fs.readFileSync(path, "utf8");
        const data = JSON.parse(text);

        let books = 0;
        let authors = [];
        let courses = [];
        let bookTitles = [];
        let authorBook = [];

        DumpPrefixes();
        NewLine();

        data.forEach(book => {
            diagnostics("Processing book: ", book.title);
            assert(BookIsValid(book));
            TrimObjectStrings(book);
            BookTeachingToRDF(book);

            books += 1;
            bookTitles.push(JSON.stringify(book));
            courses.push(book.course);
            authors = authors.concat(book.authors);

            book.authors.forEach(author => authorBook.push( { "author" : author, "book" : book.title }) );
        });
        console.log("\n");

        /* Processing of authors as entities */
        authors = RemoveDuplicates(authors);
        authors.forEach(author => AuthorToRDF(author));
        console.log("\n");

        bookTitles = RemoveDuplicates(bookTitles);
        bookTitles.forEach(book => BookToRDF(book));
        console.log("\n");

        authorBook = RemoveDuplicates(authorBook);
        authorBook.forEach(pair => AuthorBookToRDF(pair));
        console.log("\n");

        courses = RemoveDuplicates(courses);

        /* Statistics */
        console.error("\nSuccessfully parsed: ");
        console.error("Teaching Books:", books);
        console.error("Book entities:", bookTitles.length);
        console.error("Authors:", authors.length);
        console.error("Courses:", courses.length);
        console.error("Book-author:", authorBook.length);

    } catch (err) {
        console.error("Parsing failed: ", err);
    }
}

const BOOKS_PATH = "../Datasets/Books.json";

Books(BOOKS_PATH);