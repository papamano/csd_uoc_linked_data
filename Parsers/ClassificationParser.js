const fs = require("fs")

const {
    assert,
    Sanitize,
    TrimObjectStrings,
    diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

function EntryIsValid(entry) {
    assert(typeof(entry) == "object");

    /* Type check */
    result = typeof (entry.course) == "string" &&
             typeof (entry.class) == "object";

    if (!result) return false;

    /* Condition check */
    result = (entry.course.length == 6 || entry.course.length == 9) &&
             entry.class.length > 0;

    return result;
}

function EntryToRDF(entry) {
    assert(EntryIsValid(entry));

    if (entry.class[0] == "this_is_the_class") return false;

    entry.class.forEach(c => PrintRDF(Sanitize(entry.course), "csd:acmClassification <" + c + "> ."));

    return true;
}

function Classifications(path) {
    assert(typeof (path) == "string" && path);

    try {
        const text = fs.readFileSync(path, "utf8");
        const data = JSON.parse(text);

        let entries = 0;

        let courses = [];

        DumpPrefixes();
        NewLine();

        data.forEach(entry => {
            diagnostics("Processing course: ", entry.course);

            assert(!courses.includes(entry.course));
            courses.push(entry.course);

            assert(EntryIsValid(entry));
            TrimObjectStrings(entry);
            TrimObjectStrings(entry.class);

            if (EntryToRDF(entry)) {
                entries += 1;
                NewLine();
            }
        });

        /* Statistics */
        console.error("\nSuccessfully parsed: ");
        console.error("Courses:", entries);

    } catch (err) {
        console.error("Parsing failed: ", err);
    }
}

const CLASSIFICATIONS_PATH = "../Datasets/ACM_Classification.json";

Classifications(CLASSIFICATIONS_PATH);