const fs = require("fs")

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    Urlify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");


function ServiceIsValid(service) {
	assert(typeof(service) == "object");

	/* Type check */
	result = typeof( service.title == "string" ) &&
			 typeof( service.description == "string" ) &&
			 typeof( service.url == "string" ) &&
			 typeof( service.category == "string" );

	if (!result) return false;

	/* Condition check */
	result = service.title.length > 0 &&
			 service.description.length > 0 &&
			 service.url.length > 0 &&
			 service.category.length > 0;

	return result;
}

function ServiceToRDF(service) {
	assert(ServiceIsValid(service));

	let lang = (service.description.includes("e") ? LANGS.EN : LANGS.GR);

	PrintRDF(Sanitize(service.title), "rdf:type schema:Service", ";");
	PrintRDF(TAB, "rdfs:label", Stringify(service.title, LANGS.EN), ";");
	PrintRDF(TAB, "rdfs:comment", Stringify(service.description, lang), ";");
	PrintRDF(TAB, "rdfs:abstract", Stringify(service.description, lang), ";");
	PrintRDF(TAB, "schema:description", Stringify(service.description, lang), ";");
	PrintRDF(TAB, "vcard:url", Urlify(service.url), ";");
	PrintRDF(TAB, "schema:url", Urlify(service.url), ";");
	PrintRDF(TAB, "foaf:homepage", Urlify(service.url), ";");
	PrintRDF(TAB, "schema:provider csd:Computer_Science_Department", ";")
	PrintRDF(TAB, "obo:ERO_0000390 csd:Computer_Science_Department", ";")

	let type = "";
	if (service.category == "Access") type = "obo:ERO_0000391";
	else if (service.category == "Maintenance") type = "obo:ERO_0001258";
	else if (service.category == "Storage") type = "obo:ERO_0000392";
	else if (service.category == "DataStorage") type = "obo:ERO_0001257";
	else if (service.category == "Support") type = "obo:ERO_0001255";
	else if (service.category == "Training") type = "obo:ERO_0000393";
	else if (service.category == "Transport") type = "obo:ERO_0001254 ";
	else assert(false);

	PrintRDF(TAB, "rdf:type", type, ".");

	PrintRDF("csd:Computer_Science_Department obo:ERO_0000037", Sanitize(service.title),".");
}

function Services(path) {
	assert(typeof (path) == "string" && path);

	try {
		const text = fs.readFileSync(path, "utf8");
		const data = JSON.parse(text);

		let services = 0;

		DumpPrefixes();
		NewLine();

		data.forEach(service => {
			diagnostics("Processing service: ", service.title);
			assert(ServiceIsValid(service));
			TrimObjectStrings(service);
			ServiceToRDF(service);
			NewLine();
			services += 1;
		});

		/* Statistics */
		console.error("\nSuccessfully parsed: ");
		console.error("Services:", services);

	} catch (err) {
		console.error("Parsing failed: ", err);
	}
}

const SERVICES_PATH = "../Datasets/Services.json";

Services(SERVICES_PATH);