const fs = require("fs")

const {
    assert,
    Sanitize,
    TrimObjectStrings,
    diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    Urlify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");


function CourseIsValid(course) {
    assert(typeof(course) == "object");

    /* Type check */
    result = typeof (course.course_id) == "number" &&
             typeof (course.course_name) == "string" &&
             typeof (course.course_name_en) == "string" &&
             typeof (course.course_code) == "string" &&
             typeof (course.course_code_en) == "string" &&
             typeof (course.course_text) == "string" &&
             typeof (course.course_text_en) == "string" &&
             typeof (course.course_ext_url) == "string" &&
             typeof (course.course_email) == "string" &&
             typeof (course.ects) == "string" &&
             typeof (course.required) == "object" &&
             typeof (course.required_en) == "object" &&
             typeof (course.suggested) == "object" &&
             typeof (course.suggested_en) == "object" &&
             typeof (course.program_name) == "string" &&
             typeof (course.area_code) == "string" &&
             typeof (course.area_code_en) == "string" &&
             typeof (course.area_name) == "string" &&
             typeof (course.area_name_en) == "string";

    if (!result) return false;

    /* Condition check */
    result = course.course_id > 0 &&
             course.course_name.length > 0 &&
             course.course_name_en.length > 0 &&
             course.course_code.length > 0 &&
             course.course_code_en.length > 0 &&
             course.ects.length > 0 &&
             course.program_name.length > 0 &&
             course.area_name.length > 0 &&
             course.area_name_en.length > 0;

    return result;
}

function FormCourseArea(code) {
    assert(typeof(code) == "string");

    let id = "Course Area ";

    if (code.length == 0) id += "Core";
    else if (code == "Θ") id += "TH";
    else id += code;

    return id;
}

function EscapeStrings(str) {
    return str.replace(/"/g, "\\\"");
}

function CourseToRDF(course) {
    assert(CourseIsValid(course));

    PrintRDF(Sanitize(course.course_code_en), "rdf:type vivo:Course", ";");
    PrintRDF(TAB, "rdf:type schema:Course", ";");
    PrintRDF(TAB, "rdf:type schema:ContactPoint", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(course.course_name, LANGS.GR), ";");
    PrintRDF(TAB, "rdfs:label", Stringify(course.course_name_en, LANGS.EN), ";");

    if(course.course_text) {
        PrintRDF(TAB, "rdfs:comment", Stringify(EscapeStrings(course.course_text), LANGS.GR), ";");
        PrintRDF(TAB, "rdfs:abstract", Stringify(EscapeStrings(course.course_text), LANGS.GR), ";");
        PrintRDF(TAB, "schema:description", Stringify(EscapeStrings(course.course_text), LANGS.GR), ";");
    }

    if(course.course_text_en) {
        PrintRDF(TAB, "rdfs:comment", Stringify(EscapeStrings(course.course_text_en), LANGS.EN), ";");
        PrintRDF(TAB, "rdfs:abstract", Stringify(EscapeStrings(course.course_text_en), LANGS.EN), ";");
        PrintRDF(TAB, "schema:description", Stringify(EscapeStrings(course.course_text_en), LANGS.EN), ";");
    }

    PrintRDF(TAB, "schema:courseCode", Stringify(course.course_code, LANGS.GR), ";");
    PrintRDF(TAB, "bibo:identifier", Stringify(course.course_code, LANGS.GR), ";");
    PrintRDF(TAB, "schema:courseCode", Stringify(course.course_code_en, LANGS.EN), ";");
    PrintRDF(TAB, "bibo:identifier", Stringify(course.course_code_en, LANGS.EN), ";");

    if (course.course_ext_url) {
        PrintRDF(TAB, "vcard:url", Urlify(course.course_ext_url), ";");
        PrintRDF(TAB, "schema:url", Urlify(course.course_ext_url), ";");
        PrintRDF(TAB, "foaf:homepage", Urlify(course.course_ext_url), ";");
    }

    if (course.course_email) {
        PrintRDF(TAB, "schema:email", Urlify(course.course_email), ";");
    }

    assert(!isNaN(parseInt(course.ects)));
    PrintRDF(TAB, "vivo:courseCredits", parseInt(course.ects), ";");

    if (course.program_name == "Προπτυχιακό πρόγραμμα")
        PrintRDF(TAB, "csd:hasProgram csd:UndergraduateProgram", ";");
    else if (course.program_name == "Μεταπτυχιακό πρόγραμμα")
        PrintRDF(TAB, "csd:hasProgram csd:GraduateProgram", ";");
    else
        assert(false);

    PrintRDF(TAB, "schema:sourceOrganization csd:Computer_Science_Department", ".");

    PrintRDF(Sanitize(course.course_code_en), "vivo:hasSubjectArea", Sanitize(FormCourseArea(course.area_code_en)), ".");
    PrintRDF(Sanitize(FormCourseArea(course.area_code_en)), "vivo:subjectAreaOf", Sanitize(course.course_code_en), ".");

    course.required_en.forEach(x => {
        if (x == "-" ||
            x == "--" ||
            x == "---" ||
            x == "--------------------" ||
            x.length == 0) return;

        assert(x.length == 6 ||
               x.length == 7 ||
               x.length == 8);

        PrintRDF(Sanitize(course.course_code_en), "vivo:hasPrerequisite", Sanitize(x), ".");
        PrintRDF(Sanitize(course.course_code_en), "schema:coursePrerequisites", Sanitize(x), ".");
        PrintRDF(Sanitize(x), "vivo:prerequisiteFor", Sanitize(course.course_code_en), ".");
    });

    course.suggested_en.forEach(x => {
        if (x == "-" ||
            x == "--" ||
            x == "---" ||
            x == "--------------------" ||
            x.length == 0) return;

        assert(x.length == 6 ||
               x.length == 7 ||
               x.length == 8);

        PrintRDF(Sanitize(course.course_code_en), "csd:hasSuggested", Sanitize(x), ".");
        PrintRDF(Sanitize(x), "csd:suggestedFor", Sanitize(course.course_code_en), ".");
    });
}

function CourseAreaToRDF(course) {
    assert(CourseIsValid(course));

    PrintRDF(Sanitize(course.course_code_en), "vivo:hasSubjectArea", Sanitize(FormCourseArea(course.area_code_en)), ".");
    PrintRDF(Sanitize(FormCourseArea(course.area_code_en)), "vivo:subjectAreaOf", Sanitize(course.course_code_en), ".");
}

function AreaToRDF(area) {
    assert(typeof(area) == "string");

    try {
        object = JSON.parse(area);

        id = FormCourseArea(object.code);

        PrintRDF(Sanitize(id), "rdf:type csd:CourseArea", ";");
        PrintRDF(TAB, "bibo:identifier", Stringify(id), ";");
        PrintRDF(TAB, "rdfs:label", Stringify(object.name_gr, LANGS.GR), ";");
        PrintRDF(TAB, "rdfs:label", Stringify(object.name_en, LANGS.EN), ".");

        NewLine();

    } catch (err) {
        assert(false);
    }
}

function Courses(path) {
    assert(typeof (path) == "string" && path);

    try {
        const text = fs.readFileSync(path, "utf8");
        const data = JSON.parse(text);

        let processedCourses = [];
        let areas = [];

        DumpPrefixes();
        NewLine();

        data.forEach(course => {
            diagnostics("Processing course: ", course.course_code_en);

            course.required = course.required.split(/,|or|and/g);
            course.required_en = course.required_en.split(/,|or|and/g);
            course.suggested = course.suggested.split(/,|or|and/g);
            course.suggested_en = course.suggested_en.split(/,|or|and/g);

            TrimObjectStrings(course.required);
            TrimObjectStrings(course.required_en);
            TrimObjectStrings(course.suggested);
            TrimObjectStrings(course.suggested_en);

            assert(CourseIsValid(course));
            TrimObjectStrings(course);

            if (processedCourses.includes(course.course_code_en))
                CourseAreaToRDF(course);
            else {
                CourseToRDF(course);
                processedCourses.push(course.course_code_en);
            }

            areas.push(JSON.stringify({
                "code" : course.area_code_en,
                "name_gr" : course.area_name,
                "name_en" : course.area_name_en
            }));

            NewLine();
        });

        areas = RemoveDuplicates(areas);
        areas.forEach(AreaToRDF);

        /* Statistics */
        console.error("\nSuccessfully parsed: ");
        console.error("Courses:", processedCourses.length);

    } catch (err) {
        console.error("Parsing failed: ", err);
    }
}

const COURSES_PATH = "../Datasets/CSD-courses.json";

Courses(COURSES_PATH);