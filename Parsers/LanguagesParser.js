const fs = require("fs")

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
	TAB } = require("./Utilities.js");

function EntryIsValid(entry) {
	assert(typeof(entry) == "object");

	result = typeof (entry.title) == "string" &&
			 typeof (entry.languages) == "object";

	if (!result) return false;

	/* Condition check */
	result = entry.title.length >= 6 &&
			 entry.languages.length > 0;

	return result;
}

function EntryToRDF(entry) {
	assert(typeof(entry) == "object");

	if (entry.languages[0] == '-') return;

	entry.languages.forEach(language => {
		PrintRDF(Sanitize(entry.title), "schema:programmingLanguage", Sanitize(language), ".");
		PrintRDF(Sanitize(language), "schema:subjectOf", Sanitize(entry.title), ".");
	});

	NewLine();
}

function ProgrammingLanguageToRDF(language) {
	assert(typeof(language) == "string");
	PrintRDF(Sanitize(language), "rdf:type schema:ComputerLanguage", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(language, LANGS.EN), ";");
    PrintRDF(TAB, "foaf:name", Stringify(language, LANGS.EN), ".");
	NewLine();
}

function ProgrammingLanguages(path) {
	assert(typeof (path) == "string" && path);

	try {
		const text = fs.readFileSync(path, "utf8");
		const data = text.split("\n");

		let languages = [ "-" ];
		let entries = 0;

		DumpPrefixes();
		NewLine();

		data.forEach(line => {
			/* Data cleaning */
			line = line.replace("\r", "");

			diagnostics("Processing entry ", line);

			let columns = line.split(",");
			let entry = {};
			entry.title = columns[0];
			entry.languages = columns.slice(1);

			assert(EntryIsValid(entry));
			TrimObjectStrings(entry);
			TrimObjectStrings(entry.languages);
			EntryToRDF(entry);

			entries += 1;
			entry.languages.forEach(language => languages.push(language));
		});

		languages = RemoveDuplicates(languages);
		assert(languages[0] == "-");
		languages.shift();
		languages.forEach(language => ProgrammingLanguageToRDF(language));

		/* Statistics */
		console.error("\nSuccessfully parsed: ");
		console.error("Entries:", entries);
		console.error("Programming Languages:", languages.length);

	} catch (err) {
		console.error("Parsing failed: ", err);
	}
}


const LANGUAGES_PATH = "../Datasets/ProgrammingLanguage.csv";

ProgrammingLanguages(LANGUAGES_PATH);