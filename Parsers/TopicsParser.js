const fs = require("fs")

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");

function EntryIsValid(entry) {
	assert(typeof(entry) == "object");

	result = typeof (entry.title) == "string" &&
			 typeof (entry.topics) == "object";

	if (!result) return false;

	/* Condition check */
	result = entry.title.length >= 6 &&
			 entry.topics.length > 0;

	return result;
}

function EntryToRDF(entry) {
	assert(EntryIsValid(entry));

	entry.topics.forEach(topic => {
		PrintRDF(Sanitize(entry.title), "vivo:hasAssociatedConcept", Sanitize(topic), ";");
		PrintRDF(TAB, "schema:about", Sanitize(topic), ".");
	});
}

function TopicToRDF(topic) {
	assert(typeof(topic) == "string");
	assert(topic.length > 0);

    PrintRDF(Sanitize(topic), "rdf:type skos:Concept", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(topic, LANGS.EN), ".");
	NewLine();
}

function Topics(path) {
	assert(typeof (path) == "string" && path);

	try {
		const text = fs.readFileSync(path, "utf8");
		const data = text.split("\n");

		let entries = 0;
		let topics = [];

		DumpPrefixes();
		NewLine();

		data.forEach(line => {
			/* Data cleaning */
			line = line.replace("\r", "");

			diagnostics("Processing entry", line.substr(0, 6));

			let columns = line.split(",");
			let entry = {};
			entry.title = columns[0];
			entry.topics = columns.slice(1);

			assert(EntryIsValid(entry));
			TrimObjectStrings(entry);
			TrimObjectStrings(entry.topics);
			EntryToRDF(entry);
			NewLine();

			entries += 1;
			topics = topics.concat(entry.topics);
		});

		topics = RemoveDuplicates(topics);
		topics.forEach(TopicToRDF);

		/* Statistics */
		console.error("\nSuccessfully parsed: ");
		console.error("Entries:", entries);
		console.error("Topics: ", topics.length);

	} catch (err) {
		console.error("Parsing failed: ", err);
	}
}


const TOPICS_PATH = "../Datasets/CourseTopics.csv";

Topics(TOPICS_PATH);