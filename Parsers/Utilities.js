const fs = require("fs");
const default_assert = require("assert");

const TAB = "\t";

const LANGS = {
	"GR" : 0,
	"EN" : 1
};

const DEBUG_PRINT = false;

const PREFIX = "csd";

if (DEBUG_PRINT) var diagnostics = console.log;
else var diagnostics = (function() { });

function assert(expr) {
	default_assert(expr, "Assertion Failed");
}

function Sanitize(string) {
	assert(typeof (string) == "string");

    /* Remove specail characters */
	string = string.replace(/\./g, "_");
	string = string.replace(/-/g, "_");
	string = string.replace(/'/g, "_");
	string = string.replace(/\//g, "_");
	string = string.replace(/:/g, "_");
	string = string.replace(/,/g, "_");
	string = string.replace(/\+/g, "p");
	string = string.replace(/\s/g, "_");
	string = string.replace(/\(/g, "_");
    string = string.replace(/\)/g, "_");
    string = string.replace("&", "and");
    string = string.replace(/é/g, "e");

    /* Remove consecutive underscores */
    string = string.replace(/_+/g, "_");

    /* Remove whitespace */
    string = string.trim();

    /* Make sure that URIs don't end with an underscore */
    if (string.endsWith("_")) string = string.substr(0, string.length - 1);

	return PREFIX + ":" + string;
}

function TrimObjectStrings(object) {
	assert(typeof (object) == "object");

	Object.keys(object).map((key) => {
		if (typeof(object[key]) == "string")
			object[key] = object[key].trim();
	});
}

function RemoveDuplicates(array) {
	assert(typeof(array) == "object");

	return array.filter((item, index) => array.indexOf(item) == index);
}

const PREFIX_PATH = "../Triples/Prefix.ttl";

function DumpPrefixes(path) {
	if (!path) path = PREFIX_PATH;

	const text = fs.readFileSync(path, "utf8");
	console.log(text);
}

function Stringify(str, lang) {
    assert(typeof(str) == "string" && str.length > 0);

    result = ("\"" + str + "\"");

    if (lang == LANGS.GR) result += "@gr";
    else if (lang == LANGS.EN) result += "@en";

    return result;
}

function Urlify(str) {
    assert(typeof(str) == "string" && str.length > 0);
    return ("<" + str + ">");
}

module.exports = {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
	RemoveDuplicates,
	DumpPrefixes,
    Stringify,
    Urlify,
	"PrintRDF" : console.log,
	"NewLine" : (function() { console.log(" "); }),
	TAB,
	LANGS
};