#!/bin/bash

function Generate {
    #echo Producing $(basename $2)
    node $1 > $2
}

Generate DepartmentParser.js ../Triples/Department.ttl
Generate ClassroomsParser.js ../Triples/Classrooms.ttl
Generate CoursesParser.js ../Triples/Courses.ttl
Generate TopicsParser.js ../Triples/Topics.ttl
Generate LanguagesParser.js ../Triples/ProgrammingLanguages.ttl
Generate PersonsParser.js ../Triples/Persons.ttl
Generate ServicesParser.js ../Triples/Services.ttl
Generate BooksParser.js ../Triples/Books.ttl
Generate TeachingsParser.js ../Triples/Teachings.ttl
Generate ClassificationParser.js ../Triples/Classifications.ttl