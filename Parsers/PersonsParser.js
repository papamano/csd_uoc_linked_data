const fs = require("fs")

const {
	assert,
	Sanitize,
	TrimObjectStrings,
	diagnostics,
    RemoveDuplicates,
    DumpPrefixes,
    PrintRDF,
    Stringify,
    Urlify,
    NewLine,
    LANGS,
    TAB } = require("./Utilities.js");


function PersonIsValid(person) {
	assert(typeof(person) == "object");

	/* Type check */
	result = typeof (person.people_id) == "number" &&
			 typeof (person.people_name) == "string" &&
			 typeof (person.people_name_en) == "string" &&
			 typeof (person.people_email) == "string" &&
			 typeof (person.people_ext_url) == "string" &&
			 typeof (person.people_text) == "string" &&
			 typeof (person.people_text_en) == "string" &&
			 typeof (person.position_name) == "string" &&
			 typeof (person.position_name_en) == "string";

	if (!result) return false;

	/* Condition check */
	result = person.people_id > 0 &&
			 person.people_name.length > 0 &&
			 person.people_name_en.length > 0 &&
			 person.people_email.length > 10 &&
			 person.position_name.length > 0 &&
			 person.position_name_en.length > 0;

	return result;
}

function EscapeSpecialChars(str) {
	return str.replace(/\n/g, "").replace(/"/g, "\\\"");
}

function PersonToRDF(person) {
	assert(PersonIsValid(person));

    PrintRDF(Sanitize(person.people_name_en), "rdf:type schema:Person", ";");
    PrintRDF(TAB, "rdf:type foaf:Person", ";");
    PrintRDF(TAB, "rdfs:label", Stringify(person.people_name, LANGS.GR), ";");
	PrintRDF(TAB, "schema:givenName", Stringify(person.people_name.split(" ")[0].trim(), LANGS.GR), ";");
	PrintRDF(TAB, "schema:familyName", Stringify(person.people_name.split(" ")[1].trim(), LANGS.GR), ";");
	PrintRDF(TAB, "rdfs:label", Stringify(person.people_name_en, LANGS.EN), ";");
	PrintRDF(TAB, "schema:givenName", Stringify(person.people_name_en.split(" ")[0].trim(), LANGS.EN), ";");
	PrintRDF(TAB, "schema:familyName", Stringify(person.people_name_en.split(" ")[1].trim(), LANGS.EN), ";");
	PrintRDF(TAB, "rdf:type vivo:FacultyMember", ";");
	PrintRDF(TAB, "schema:email", Urlify(person.people_email), ";");
	PrintRDF(TAB, "schema:worksFor", "csd:Computer_Science_Department", ";");
	PrintRDF(TAB, "schema:worksFor", "csd:University_of_Crete", ";");
	PrintRDF(TAB, "schema:affiliation", "csd:Computer_Science_Department", ";");
	PrintRDF(TAB, "schema:affiliation", "csd:University_of_Crete", ";");
	PrintRDF(TAB, "schema:memberOf", "csd:Computer_Science_Department", ";");
	PrintRDF(TAB, "schema:memberOf", "csd:University_of_Crete", ";");
	PrintRDF(TAB, "schema:nationality", "csd:Greece", ";");

	if (person.people_ext_url) {
		PrintRDF(TAB, "schema:url", Urlify(person.people_ext_url), ";");
		PrintRDF(TAB, "vcard:url", Urlify(person.people_ext_url), ";");
		PrintRDF(TAB, "foaf:homepage", Urlify(person.people_ext_url), ";");
		PrintRDF(TAB, "foaf:workplaceHomepage", Urlify(person.people_ext_url), ";");
	}

	if (person.text) {
		PrintRDF(TAB, "rdfs:abstract", Stringify(EscapeSpecialChars(person.people_text), LANGS.GR), ";");
		PrintRDF(TAB, "schema:description", Stringify(EscapeSpecialChars(person.people_text), LANGS.GR), ";");
	}

	if (person.people_text_en) {
		PrintRDF(TAB, "rdfs:abstract", Stringify(EscapeSpecialChars(person.people_text_en), LANGS.EN), ";");
		PrintRDF(TAB, "schema:description", Stringify(EscapeSpecialChars(person.people_text_en), LANGS.EN), ";");
	}

	let owlClass = "";
	let position = "";

	if(person.position_name_en == "Professors") {
		owlClass = "csd:FullProfessor";
		position = "csd:FullProfessorPosition";
	} else if (person.position_name_en == "Associate Professors") {
		owlClass = "csd:AssociateProfessor";
		position = "csd:AssociateProfessorPosition";
	} else if (person.position_name_en == "Assistant Professors") {
		owlClass = "csd:AssistantProfessor";
		position = "csd:AssistantProfessorPosition";
	} else if (person.position_name_en == "Visiting Professors") {
		owlClass = "csd:VisitingProfessor";
		position = "csd:VisitingProfessorPosition";
	} else if (person.position_name_en == "E.D.I.P") {
		owlClass = "csd:LabStaff";
		position = "csd:LabStaffPosition";
	} else if (person.position_name_en == "Secretary") {
		owlClass = "csd:SecretaryStaff";
		position = "csd:SecretaryStaffPosition";
	} else if (person.position_name_en == "E.T.E.P") {
		owlClass = "csd:SystemStaff";
		position = "csd:SystemStaffPosition";
	}
	else assert(false);

	PrintRDF(TAB, "rdf:type", owlClass, ";");
	PrintRDF(TAB, "vcard:hasRole", position, ";");
	PrintRDF(TAB, "vivo:contributingRole", position, ".");
}

function Persons(path) {
	assert(typeof (path) == "string" && path);

	try {
		const text = fs.readFileSync(path, "utf8");
		const data = JSON.parse(text);

		let persons = 0;

		DumpPrefixes();
		NewLine();

		data.forEach(person => {
			diagnostics("Processing person: ", person.people_name_en);
			assert(PersonIsValid(person));
			TrimObjectStrings(person);
			PersonToRDF(person);
			NewLine();

			persons += 1;
		});

		/* Statistics */
		console.error("\nSuccessfully parsed: ");
		console.error("Persons:", persons);

	} catch (err) {
		console.error("Parsing failed: ", err);
	}
}

const PERSONS_PATH = "../Datasets/CSD-people.json";

Persons(PERSONS_PATH);