'use strict';

/* The required modules (dependencies) */
const express   = require('express');
const router    = express.Router();
const port      = require('../configs/config').httpPort;
const ip        = require('../configs/config').ip;
const endpoint  = require('../configs/config').sparqlEndpoint;
const queries   = require('../configs/queries');
const log       = require('../helpers/logger.js').Log;
const FormQuery = require('../helpers/queries.js').FormSparqlQuery;
const assert    = require('assert');

const VISITS_LOG_INTERVAL = 5;

const Statistics = {
    "homepage" : 0
};

function IncreaseVisits(key) {
    assert(typeof(key) == "string" && key.length > 0);

    try {

        if (!Statistics[key]) Statistics[key] = 0;
        Statistics[key] += 1;

        if (Statistics[key] % VISITS_LOG_INTERVAL == 0) log(Statistics[key] + " visits to " + key);
    }
    catch(error) {
        assert(false);
    }
}

/* GET request for the homepage. Redirect to device manager */
router.get('/', function (req, res, next) {
    IncreaseVisits("homepage");

    // Render homepage
    res.render('homepage.ejs', { "queries" : queries, "endpoint" : endpoint });
});

router.get('/:subject', function (req, res, next) {
    let subject = req.params.subject;

    res.render('subject.ejs', { "subject" : req.params.subject } );

    IncreaseVisits(subject);
});

router.get('/visual/:subject', function (req, res, next) {
    let subject = "visual/" + req.params.subject;

    res.render('visualize.ejs', { "subject" : req.params.subject } );

    IncreaseVisits(subject);
});

router.get('/describe/:subject', function (req, res, next) {
    let subject = "describe/" + req.params.subject;

    // Redirect the user to the correct page
    res.redirect(FormQuery("DESCRIBE csd:" + req.params.subject, endpoint));

    IncreaseVisits(subject);
});

module.exports = router;