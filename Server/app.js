'use strict';

/* The required modules (dependencies) */
const express = require('express');
const app     = express();
const routes  = require('./routes');
const path    = require('path');
const http    = require('http').Server(app);
const port    = require('./configs/config').httpPort;

// View engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Middlewares
app.use(express.static(path.join(__dirname, './public')));
app.use('/', routes);

/* 404 responses are not the result of an error, so the error-handler middleware
 * will not capture them (Express has executed all middleware functions and
 * routes, and found that none of them responded).
 * Catch it and forward it to the error handler */
app.use(function(req, res, next) {
    var err = new Error('File Not Found');
    err.status = 404;
    next(err);
});

/* Error handler middleware (Should be defined as the last app.use callback) */
app.use(function(err, req, res, next) {
    /* Status Code 500: Internal Server Error */
    res.status(err.status || 500);
    res.render('error404.ejs', {
        message: err.message,
        error: {}
    });
});

/* Create a server and listen to a specific port */
http.listen(port, () => console.log("Server running on port: " + port));

// Use sockets
const io = require('./sockets')(http);