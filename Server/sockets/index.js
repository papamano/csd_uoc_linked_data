'use strict';

/* The required modules (dependencies) */
const socketio = require('socket.io');

function socket(server) {
    let io = socketio.listen(server, { rejectUnauthorized: true } );

    // On new socket connection
    io.on('connection', function(socket){
        console.log('New socket connection accepted: ' + socket.id);
        // Socket functionality (register events)
        require('./events')(io, socket)
    });

    return io;
}

module.exports = socket;