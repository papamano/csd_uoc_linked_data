"use strict";

const assert = require("assert");

module.exports = function(io, socket) {
    console.log("Registering socket events");

    /* Event handler for when the client disconnects */
    socket.on("disconnect", () => console.log("Socket disconnected: " + socket.id));

    /* Register different type of events every time a new socket is created */
    require("./triplesEvents.js")(socket, io);
}