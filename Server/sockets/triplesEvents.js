"use strict";

const assert = require("assert");

const { QuerySubject,
    QueryObject,
    CountTriples } = require("../helpers/sparqlClient.js");

function SendTriplesSubject(socket, subject) {
    assert(typeof (socket) == "object" && typeof (socket.emit) == "function");

    QuerySubject("csd:" + subject)
        .then(data => socket.emit("load_triples_subject_resp", data))
        .catch(err => assert(false));
}

function SendTriplesObject(socket, subject) {
    assert(typeof (socket) == "object" && typeof (socket.emit) == "function");

    QueryObject("csd:" + subject)
        .then(data => socket.emit("load_triples_object_resp", data))
        .catch(err => assert(false));
}

function SendEntityStats(socket, subject) {
    assert(typeof (socket) == "object" && typeof (socket.emit) == "function");
    assert(typeof (subject) == "string" && subject.length > 0);

    let stats = {
        "subject": 0,
        "object": 0,
        "csdPropertySubject": 0,
        "csdPropertyObject": 0,
        "csdEntities": 0
    };

    QuerySubject("csd:" + subject)
    .then(data => {

        stats.subject = data.length;

        data.forEach(pair => {
            if (pair.property.startsWith("csd:")) stats.csdPropertySubject += 1;
            if (pair.object.startsWith("csd:")) stats.csdEntities += 1;
        });

        return "csd:" + subject;
    })
    .then(QueryObject)
    .then(data => {

        stats.object = data.length;

        data.forEach(pair => {
            if (pair.property.startsWith("csd:")) stats.csdPropertyObject += 1;
            if (pair.subject.startsWith("csd:")) stats.csdEntities += 1;
        });

        return stats;
    })
    .then(data => {
        socket.emit("load_entity_stats_resp", data);
    })
    .catch(err => console.warn(err));
}

module.exports = function (socket, io) {
    assert(typeof (socket) === "object");
    assert(typeof (io) === "object");
    assert(typeof (io.emit) === "function");

    /* Load data about a specific entity */
    socket.on("load_data", data => {
        assert(typeof(data) == "object");
        SendTriplesSubject(socket, data.subject);
        SendTriplesObject(socket, data.subject);
    });

    socket.on("load_triples_subject", data => {
        assert(typeof(data) == "object");
        SendTriplesSubject(socket, data.subject);
    });

    socket.on("load_triples_object", data => {
        assert(typeof(data) == "object");
        SendTriplesObject(socket, data.subject);
    });

    socket.on("load_entity_stats", data => {
        assert(typeof(data) == "object");
        SendEntityStats(socket, data.subject);
    })

    /* Get number of triples in triplestore */
    socket.on("triples_count", _ => {
        CountTriples()
            .then(number => socket.emit("triples_count_resp", {
                "count": number
            }))
            .catch(err => assert(false));
    });
}
