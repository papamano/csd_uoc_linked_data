"use strict";

const fs      = require('fs');
const process = require('process');
const assert  = require('assert');

const MAX_LINES = 500;

/* Adds a 0 before numbers less than 10 */
function MinTwoDigits(n) {
    return (n < 10 ? '0' : '') + n;
}

/* Returns date in specific format: [ DD/MM/YYYY HH:MM:SS ] */
function GetDate() {
    let now = new Date();
    let date = "[ " + MinTwoDigits(now.getDate()) + "/" + MinTwoDigits(now.getMonth() + 1) + "/" + MinTwoDigits(now.getFullYear());
    date += " " + MinTwoDigits(now.getHours()) + ":" + MinTwoDigits(now.getMinutes()) + ":" + MinTwoDigits(now.getSeconds()) + " ] ";
    return date;
}

/* Returns info about the current environment:
 * PID
 * Platform
 * Uptime (secs)
 */
function GetInfo() {
    let info = "[ " + process.pid + " ] [ "
        + process.platform + " ] [ "
        + Math.floor(process.uptime()) + " secs ] ";
    return info;
}

/* Log the given text to the given file. Format is:
 * [ DD/MM/YYYY HH:MM:SS ] [ PID ] [ Platform ] [ Uptime ] Text */
function LogTo(file, text) {
    assert(typeof(file) == "string" && file.length > 0);
    assert(typeof(text) == "string" && text.length > 0);

    let str = GetDate() + GetInfo() + String(text).replace("\n", " ") + "\n";
    let lines;
    let buffer = "";

    fs.appendFile(file, str, (err) => {
        if (err) return console.warn(err);

        fs.createReadStream(file).on("data", (chunk) => buffer += chunk)
            .on("end", () => {
                lines = String(buffer).split("\n");
                if (lines.length - 1 > MAX_LINES) {
                    lines.splice(0, lines.length - 1 - MAX_LINES);
                    fs.writeFile(file, lines.join("\n"), () => { });
                }
            });
    });
}

module.exports = {
    "Log" : function(text) { LogTo(__dirname + "/../info.log", text); }
}