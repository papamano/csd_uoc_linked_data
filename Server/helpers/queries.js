const assert  = require('assert');

function FormSparqlQuery(text, endpoint) {
    assert(typeof(text) == "string" && text.length > 0);
    assert(typeof(endpoint) == "string" && endpoint.length > 0);

    let query = "PREFIX csd: <https://www.csd.uoc.gr/~papamano/>\n";

    query += text;

    query = query.replace(/ /g, "+");
    query = query.replace(/:/g, "%3A");
    query = query.replace(/\//g, "%2F");
    query = query.replace(/</g, "%3C");
    query = query.replace(/>/g, "%3E");
    query = query.replace(/&gt;/g, "%3E");
    query = query.replace(/&lt;/g, "%3C");
    query = query.replace(/&amp;/g, "%26");
    query = query.replace(/\n/g, "%0D%0A");
    query = query.replace(/#/g, "%23");
    query = query.replace(/{/g, "%7B");
    query = query.replace(/}/g, "%7D");
    query = query.replace(/\(/g, "%28");
    query = query.replace(/\)/g, "%29");
    query = query.replace(/,/g, "%2C");
    query = query.replace(/\?/g, "%3F");
    query = query.replace(/;/g, "%3B");
    query = query.replace(/=/g, "%3D");
    query = query.replace(/&/g, "%26");

    /* Remove consecutive '+' signs */
    query = query.replace(/\++/g, "+");

    let url = endpoint + "?default-graph-uri=&query=" + query + "&format=text%2Fhtml&timeout=0&debug=on";

    return url;
}

module.exports = {
    FormSparqlQuery
};