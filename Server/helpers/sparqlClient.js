"use strict";

const { Client, Triple } = require('virtuoso-sparql-client');
const assert             = require("assert");
const graph              = require('../configs/config').graphURI;
const sparqlClient       = require('../configs/config').sparqlEndpoint;
const prefixes           = require('../configs/config').prefixes;

const PRINT_QUERY = false;
const CACHE_TRIPLES_COUNT = false;
const CACHE_TIMEOUT_PERIOD = 60; // Measured in seconds

let CACHE = {
    "lastSubject" : null,
    "cachedSubjectResult" : null,
    "lastObject" : null,
    "cachedObjectResult" : null,
    "triplesCount" : -1
};

const client = new Client(sparqlClient);

client.setDefaultPrefixes(prefixes);
client.setQueryGraph(graph);
client.setOptions("application/json");

function RenameWithPrefixes(string) {
    /* No need for assertion here. An empty string is a valid object */
    if (string.length == 0) return string;

    for (var key in prefixes) {
        if (string.startsWith(prefixes[key]))
            return string.replace(prefixes[key], key + ":");
    }

    return string;
}

function PrintLiteral(literal) {
    let string = "\"" + literal.value + "\"";
    if (literal["xml:lang"] == "en") string += "@en";
    else if (literal["xml:lang"] == "gr") string += "@gr";

    return string;
}

function Query(query) {
    assert(typeof (query) == "string" && query.length > 0);

    return new Promise((resolve, reject) => {
        client.query(query, PRINT_QUERY)
            .then(resolve)
            .catch(reject);
    });
}

function QuerySubject(subject) {
    assert(typeof (subject) == "string" && subject.length > 0);

    const query = "SELECT * WHERE { " + subject + " ?p ?o . }";

    return new Promise((resolve, reject) => {

        if (subject === CACHE.lastSubject) {
            assert(typeof(CACHE.cachedSubjectResult) == "object" && CACHE.cachedSubjectResult);
            return resolve(CACHE.cachedSubjectResult);
        }

        Query(query)
            .then((results) => {
                let data = [];

                results.results.bindings.forEach(triple => {
                    let property = RenameWithPrefixes(triple.p.value);
                    let object = "";

                    if (triple.o.type == "literal") object = PrintLiteral(triple.o);
                    else object = RenameWithPrefixes(triple.o.value);

                    data.push({
                        "property": property,
                        "object": object
                    });
                });

                CACHE.lastSubject = subject;
                CACHE.cachedSubjectResult = data;

                resolve(data);
            })
            .catch(reject);
    });
}

function QueryObject(object) {
    assert(typeof (object) == "string" && object.length > 0);

    const query = "SELECT * WHERE { ?s ?p " + object + " . }";

    return new Promise((resolve, reject) => {

        if (object === CACHE.lastObject) {
            assert(typeof(CACHE.cachedObjectResult) == "object" && CACHE.cachedObjectResult);
            return resolve(CACHE.cachedObjectResult);
        }

        Query(query)
            .then((results) => {
                let data = [];

                results.results.bindings.forEach(triple => {
                    let subject = RenameWithPrefixes(triple.s.value);
                    let property = RenameWithPrefixes(triple.p.value);

                    data.push({
                        "subject": subject,
                        "property": property
                    });
                });

                CACHE.lastObject = object;
                CACHE.cachedObjectResult = data;

                resolve(data);
            })
            .catch(reject);
    });
}

function CountTriples() {
    const query = "SELECT count(*) as ?number WHERE { ?s ?p ?o . }";

    return new Promise((resolve, reject) => {

        if (CACHE_TRIPLES_COUNT && CACHE.triplesCount >= 0) return resolve(CACHE.triplesCount);

        Query(query)
            .then((results) => {
                assert(typeof(results) == "object");
                assert(typeof(results.results) == "object");
                assert(typeof(results.results.bindings) == "object");
                assert(results.results.bindings.length >= 1);

                let number = parseInt(results.results.bindings[0].number.value);
                assert(!isNaN(number));

                resolve(number);

                if (CACHE_TRIPLES_COUNT) {
                    CACHE.triplesCount = number;

                    /* After sometime invalidate the cache and fetch new results */
                    setTimeout(_ => CACHE.triplesCount = -1, CACHE_TIMEOUT_PERIOD * 1000);
                }
            })
            .catch(reject);
    });
}

module.exports = {
    Query,
    QuerySubject,
    QueryObject,
    CountTriples
};