const assert = console.assert;
const OntologyURI = "http://localhost:8080/";

/* Global vars */
var socket = null;

var Visualizer = function(containerID) {
    assert(typeof(containerID) == "string");

    var nodes = new vis.DataSet();
    var edges = new vis.DataSet();
    var nodeId = 0;
    var container = document.getElementById(containerID);
    var network = null;
    var nodeColor = "#99ccff";

    function SetNodeColor(color) {
        assert(typeof(color) === "string");
        assert(color.length === 7);
        assert(color[0] === "#")

        nodeColor = color;
    }

    function AddNewNode(label, color, shape) {
        assert(typeof (nodes) == "object");
        assert(typeof (label) == "string");

        nodes.add({
            "id": ++nodeId,
            "label": label.substr(0, 50),
            "title" : label,
            "color" : (typeof(color) == "string" && color.length == 7) ? color : nodeColor,
            "shape" : (typeof(shape) == "string") ? shape : "ellipse"
        });

        return nodeId;
    }

    function AddNewEdge(from, to, label) {
        assert(typeof (edges) == "object");
        assert(typeof (from) == "number");
        assert(typeof (to) == "number");
        assert(typeof (label) == "string");

        edges.add({
            "from": from,
            "to": to,
            "label": label
        });
    }

    function Init() {
        var options = {
            configure: {
                enabled: false,
                showButton: true
        },
            autoResize: true,
            height: '100%',
            width: '100%',
            layout: {
                improvedLayout: true,
            },
            edges: {
                arrows: {
                    to: {
                        enabled: true
                    }
                }
            },
            nodes : {
                color : {
                    background: nodeColor
                }
            },
            physics : {
                enabled : true,
                repulsion: {
                    nodeDistance: 500,
                    avoidOverlap: 1
                  },
                  solver : "repulsion"
            }
        };

        var data = {
            nodes: nodes,
            edges: edges
        };

        network = new vis.Network(container, data, options);
    }

    function Display() {
        var data = {
            nodes: nodes,
            edges: edges
        };

        network.setData(data);
        network.redraw();
    }

    return {
        Init,
        AddNewNode,
        AddNewEdge,
        Display,
        SetNodeColor
    }
}

function init(subject) {
    /* Create a new socket object and connect to the server */
    socket = io.connect();

    visualizer = new Visualizer("mynetwork");
    visualizer.Init();

    /* When the client connects, load information about the device */
    socket.on("connect", _ => {
        console.log("Connected to server");

        socket.emit("load_triples_subject", {
            "subject": subject
        });
    });

    socket.on("load_triples_subject_resp", data => {

        visualizer.AddNewNode(subject, false, "circle");

        data.forEach(triple => {
            let newId = visualizer.AddNewNode(triple.object);
            visualizer.AddNewEdge(1, newId, triple.property);
        });

        visualizer.Display();

        socket.emit("load_triples_object", {
            "subject": subject
        });
    });

    socket.on("load_triples_object_resp", data => {

        data.forEach(triple => {
            let newId = visualizer.AddNewNode(triple.subject.substr(0, 50), "#008080");
            visualizer.AddNewEdge(newId, 1, triple.property);
        });

        visualizer.Display();
    });
}