const assert = console.assert;

/* Global vars */
var socket = null;
var sparqlEndpoint = null;

function Popup(text, title) {
    assert(typeof (text) == "string" && text.length > 0);
    assert(typeof (title) == "string" && title.length > 0);

    let modalText = document.getElementById("modal_text");
    let modalTitle = document.getElementById("modal_title");
    assert(modalText && modalTitle);

    modalText.innerHTML = text;
    modalTitle.innerHTML = title;

    assert(document.getElementById("popupModal"));
    $('#popupModal').modal();
}

function CopyToClipboard(elementID) {
    assert(typeof (elementID) == "string" && elementID.length > 0);

    let elem = document.getElementById(elementID);
    assert(elem);

    var text = elem.innerHTML;
    assert(typeof(text) == "string" && text.length > 0);

    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = text;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);

    Popup(text.replace(/\n/g, "<br>"), "Query copied to clipboard.<br>If the process failed you can copy the query here:");
}

function FormSparqlQuery(text) {
    assert(typeof(text) == "string" && text.length > 0);
    assert(typeof(sparqlEndpoint) == "string" && sparqlEndpoint.length > 0);

    let query = "DEFINE input:same-as \"yes\"\
    PREFIX foaf:   <http://xmlns.com/foaf/0.1/>\n\
    PREFIX vcard:  <http://www.w3.org/2006/vcard/ns#>\n\
    PREFIX obo:    <http://purl.obolibrary.org/obo/>\n\
    PREFIX bibo:   <http://purl.org/ontology/bibo/>\n\
    PREFIX owl:    <http://www.w3.org/2002/07/owl#>\n\
    PREFIX rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n\
    PREFIX rdfs:   <http://www.w3.org/2000/01/rdf-schema#>\n\
    PREFIX xsd:    <http://www.w3.org/2001/XMLSchema#>\n\
    PREFIX dc:     <http://purl.org/dc/elements/1.1/>\n\
    PREFIX skos:   <http://www.w3.org/2004/02/skos/core#>\n\
    PREFIX vivo:   <http://vivoweb.org/ontology/core#>\n\
    PREFIX schema: <http://schema.org/>\n\
    PREFIX csd:    <https://www.csd.uoc.gr/~papamano/>\n\
    PREFIX dbo:    <http://dbpedia.org/ontology/>\n\
    PREFIX dbr:    <http://dbpedia.org/resource/>\n\
    PREFIX acm:    <http://acm.rkbexplorer.com/ontologies/acm#>\n\n";

    query += text;

    query = query.replace(/ /g, "+");
    query = query.replace(/:/g, "%3A");
    query = query.replace(/\//g, "%2F");
    query = query.replace(/</g, "%3C");
    query = query.replace(/>/g, "%3E");
    query = query.replace(/&gt;/g, "%3E");
    query = query.replace(/&lt;/g, "%3C");
    query = query.replace(/&amp;/g, "%26");
    query = query.replace(/\n/g, "%0D%0A");
    query = query.replace(/#/g, "%23");
    query = query.replace(/{/g, "%7B");
    query = query.replace(/}/g, "%7D");
    query = query.replace(/\(/g, "%28");
    query = query.replace(/\)/g, "%29");
    query = query.replace(/,/g, "%2C");
    query = query.replace(/\?/g, "%3F");
    query = query.replace(/;/g, "%3B");
    query = query.replace(/=/g, "%3D");
    query = query.replace(/&/g, "%26");

    /* Remove consecutive '+' signs */
    query = query.replace(/\++/g, "+");

    let url = sparqlEndpoint + "?default-graph-uri=&query=" + query + "&format=text%2Fhtml&timeout=0&debug=on";

    return url;
}

function RunQuery(elementID) {
    assert(typeof (elementID) == "string" && elementID.length > 0);

    let elem = document.getElementById(elementID);
    assert(elem);

    var text = elem.innerHTML;
    assert(typeof(text) == "string" && text.length > 0);

    let url = FormSparqlQuery(text);

    var win = window.open(url, '_blank');
    win.focus();
}

function init(endpoint) {
    assert(typeof(endpoint) == "string" && endpoint.length > 0);

    sparqlEndpoint = endpoint;

    /* Create a new socket object and connect to the server */
    socket = io.connect();

    /* When the client connects, load information about the device */
    socket.on("connect", _ => {
        console.log("Connected to server");

        socket.emit("triples_count");
    });

    socket.on("triples_count_resp", data => {
        assert(typeof (data) == "object");
        assert(typeof (data.count) == "number");

        let elem = document.getElementById("countTriples");
        assert(elem);
        elem.innerHTML = data.count;
    });
}