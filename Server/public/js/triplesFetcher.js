const assert = console.assert;
const OntologyURI = "http://localhost:8080/";

/* Global vars */
const CHANGE_PAGE = false;

var socket = null;
var lineBackground = false;
var subjectTriplesSize = -1;
var objectTriplesSize = -1;
var entityStatistics = {
    "subject": 0,
    "object": 0,
    "csdPropertySubject": 0,
    "csdPropertyObject": 0,
    "csdEntities": 0,
    "literals" : 0
};

function IsLiteral(entry) {
    assert(typeof(entry) == "string");

    if (entry[0] == '"' ||
        !isNaN(parseInt(entry)) ||
        !isNaN(parseFloat(entry)) ||
        false) // TODO: More checks for dates, etc
        return true;
    return false;
}

function IsURI(entry) {
    assert(typeof(entry) == "string");

    return (entry.includes(":") || entry.includes("@"));
}

function CreateLiteral(entry) {
    assert(typeof(entry) == "string" && entry.length > 0);

    if (entry.endsWith("@gr") ||
        entry.endsWith("@en")) {

        lang = entry.substr(entry.length - 2);
        entry = entry.substr(0, entry.length - 3);
        entry += " <small><b>(" + lang + ")</b></small>";
    }

    let text = document.createElement("span");
    assert(text);
    text.innerHTML = entry;

    return text;
}

function UnwrapPrefix(entry) {
    assert(typeof(entry) == "string" && entry.length > 0);

    let result = "";

    if (entry.startsWith("foaf")) result = "http://xmlns.com/foaf/0.1/" + entry.split(":")[1].trim();
    else if (entry.startsWith("vcard")) result = "http://www.w3.org/2006/vcard/ns#" + entry.split(":")[1].trim();
    else if (entry.startsWith("obo")) result = "http://purl.obolibrary.org/obo/" + entry.split(":")[1].trim();
    else if (entry.startsWith("bibo")) result = "http://purl.org/ontology/bibo/" + entry.split(":")[1].trim();
    else if (entry.startsWith("owl")) result = "http://www.w3.org/2002/07/owl#" + entry.split(":")[1].trim();
    else if (entry.startsWith("rdf")) result = "http://www.w3.org/1999/02/22-rdf-syntax-ns#" + entry.split(":")[1].trim();
    else if (entry.startsWith("rdfs")) result = "http://www.w3.org/2000/01/rdf-schema#" + entry.split(":")[1].trim();
    else if (entry.startsWith("xsd")) result = "http://www.w3.org/2001/XMLSchema#" + entry.split(":")[1].trim();
    else if (entry.startsWith("dc")) result = "http://purl.org/dc/elements/1.1/" + entry.split(":")[1].trim();
    else if (entry.startsWith("skos")) result = "http://www.w3.org/2004/02/skos/core#" + entry.split(":")[1].trim();
    else if (entry.startsWith("vivo")) result = "http://vivoweb.org/ontology/core#" + entry.split(":")[1].trim();
    else if (entry.startsWith("schema")) result = "http://schema.org/" + entry.split(":")[1].trim();
    else if (entry.startsWith("dbpedia")) result = "http://dbpedia.org/resource/" + entry.split(":")[1].trim();
    else if (entry.startsWith("dbo")) result = "http://dbpedia.org/ontology/" + entry.split(":")[1].trim();
    else if (entry.startsWith("dbp")) result = "http://dbpedia.org/property/" + entry.split(":")[1].trim();
    else if (entry.startsWith("acm")) result = "http://acm.rkbexplorer.com/ontologies/acm#" + entry.split(":")[1].trim();
    else if (entry.startsWith("csd") && entry.includes(":")) result = OntologyURI + entry.split(":")[1].trim();
    else result = entry;

    assert(typeof(result) == "string" && result.length > 8);

    return result;
}

function CreateURI(entry) {
    assert(typeof(entry) == "string" && entry.length > 0);

    let link = document.createElement("a");
    assert(link);

    link.appendChild(CreateLiteral(entry));
    link.href = UnwrapPrefix(entry);

    return link;
}

function CreateElement(entry) {
    assert(typeof(entry) == "string");

    if (IsLiteral(entry)) return CreateLiteral(entry);
    else if (IsURI(entry)) return CreateURI(entry);
    else assert(false);
}

function CreateElementWithClass(elem, str) {
    assert(typeof(elem) == "string" && elem.length > 0);
    assert(typeof(str) == "string" && str.length > 0);

    let element = document.createElement(elem);
    assert(element);

    element.className = str;

    return element;
}

function CreateDivWithClass(str) {
    return CreateElementWithClass("div", str);
}

function CreateRow(parent, property, object, IsObject) {
    let bg = (lineBackground = !lineBackground) ? " bg-light" : "";
    let col1 = CreateDivWithClass("py-2 col-4" + bg);
    let col2 = CreateDivWithClass("py-2 col" + bg);

    if (property && typeof(IsObject) == "boolean" && IsObject) col1.append("is ");
    if (property) col1.appendChild(CreateElement(property));
    if (property && typeof(IsObject) == "boolean" && IsObject) col1.append(" of ");
    col2.appendChild(CreateElement(object));

    parent.appendChild(col1);
    parent.appendChild(col2);
    parent.appendChild(CreateElementWithClass("hr", "w-100 my-0"));
}

function RedirectTo(url) {
    assert(typeof(url) == "string" && url.length > 0);

    if (CHANGE_PAGE) {
        window.location.href = url;
    } else {
        var win = window.open(url, '_blank');
        win.focus();
    }
}

function Visualize(subject) {
    assert(typeof(subject) == "string" && subject.length > 0);
    RedirectTo("http://localhost:8080/visual/" + subject);
}

function Describe(subject) {
    assert(typeof(subject) == "string" && subject.length > 0);
    RedirectTo("http://localhost:8080/describe/" + subject);
}

function Popup(text, title) {
    assert(typeof (text) == "string" && text.length > 0);
    assert(typeof (title) == "string" && title.length > 0);

    let modalText = document.getElementById("modal_text");
    let modalTitle = document.getElementById("modal_title");
    assert(modalText && modalTitle);

    modalText.innerHTML = text;
    modalTitle.innerHTML = title;

    assert(document.getElementById("popupModal"));
    $('#popupModal').modal();
}

function Stats(subject) {
    assert(typeof(subject) == "string" && subject.length > 0);

    let text = "This entity is:<br><ul>";

    text += "<li>The subject of " + entityStatistics.subject + " triples</li>";
    text += "<li>The object of " + entityStatistics.object + " triples</li>";
    text += "<li>The subject of " + entityStatistics.csdPropertySubject + " CSD properties</li>";
    text += "<li>The object of " + entityStatistics.csdPropertyObject + " CSD properties</li>";
    text += "<li>Directly linked with " + entityStatistics.csdEntities + " other CSD entities</li>";
    text += "<li>Directly linked with " + entityStatistics.literals + " literals</li>";

    text += "</ul>";

    Popup(text, subject + " Statistics");
}

function ValidateEntityExistance() {
    if (objectTriplesSize == 0 &&
        subjectTriplesSize == 0)
        document.getElementById("panel").innerHTML = "No information for required entity";
}

function init(subject) {
    assert(typeof(subject) == "string" && subject.length > 0);

    /* Create a new socket object and connect to the server */
    socket = io.connect();

    /* When the client connects, load information about the device */
    socket.on("connect", _ => {
        console.log("Connected to server");

        socket.emit("load_data", {
            "subject" : subject
        });
    });

    socket.on("load_triples_subject_resp", data => {
        let panel = document.getElementById("triples_subjects");
        assert(panel);
        panel.innerHTML = "";

        subjectTriplesSize = data.length;

        entityStatistics.subject = data.length;

        data.sort();
        let prev = "";

        data.forEach(triple => {
            CreateRow(panel, (triple.property == prev) ? "" : triple.property, triple.object);
            prev = triple.property;

            if (triple.property.startsWith("csd:")) entityStatistics.csdPropertySubject += 1;
            if (triple.object.startsWith("csd:")) entityStatistics.csdEntities += 1;
            if (IsLiteral(triple.object)) entityStatistics.literals += 1;
        });

        ValidateEntityExistance();
    });

    socket.on("load_triples_object_resp", data => {
        let panel = document.getElementById("triples_object");
        assert(panel);
        panel.innerHTML = "";

        objectTriplesSize = data.length;

        entityStatistics.object = data.length;

        data.sort();
        let prev = "";

        lineBackground = false;
        data.forEach(triple => {
            CreateRow(panel, (triple.property == prev) ? "" : triple.property, triple.subject, true);
            prev = triple.property;

            if (triple.property.startsWith("csd:")) entityStatistics.csdPropertyObject += 1;
            if (triple.subject.startsWith("csd:")) entityStatistics.csdEntities += 1;
        });

        ValidateEntityExistance();
    });

    socket.on("load_entity_stats_resp", data => {
        assert(typeof(data) == "object");
        entityStatistics = data;
    });
}